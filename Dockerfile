FROM alpine/helm:3.10.2 as helm
FROM bitnami/kubectl:1.27.2 as kubectl

FROM alpine:3.18

COPY --from=helm /usr/bin/helm /usr/bin/helm
COPY --from=kubectl /opt/bitnami/kubectl/bin/kubectl /usr/bin/kubectl